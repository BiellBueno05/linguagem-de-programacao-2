<?php
class Pessoa {
    public $nome;

    public function __construct(string $nome)
    {
        $this->nome = $nome;
    }

    function __toString(): string 
    {
        return "Pessoa {nome: $this->nome}";
    }
}

$pessoa = new Pessoa('Rodolfo');
$pessoa1 = new Pessoa('Carvalho');

echo $pessoa ,'<br>' ,$pessoa1, "<br>";