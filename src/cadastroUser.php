<?php

interface CadastroUserInterface
{
    public function getUser();
    public function getEmail();
}

class cadastroUser implements CadastroUserInterface
{
    public function __construct(public string $userName, public string $email)
    {
    }

    public function getUser()
    {
        return $this->userName;
    }

    public function getEmail()
    {
        return $this->email;
    }
}


$user = new cadastroUser("Rodolfo Carvalho", "email@teste.com");

var_dump($user);
