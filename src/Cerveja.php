<?php


class Cerveja
{
    public function __construct(
        public string $nome
    ) {
    }

    public function __toString(){
        return $this->nome;
    }
}

class Avaliacao
{
    public function __construct(public string $estado, public float $preco, public float $nota)
    {
    }
}

class Repositorio
{
    public function __construct(
        public array $avaliacoes = [],

    ) {
    }


    public function addAvaliacao(Cerveja $cerveja, Avaliacao $avaliacao)
    {
        $id = (string)$cerveja;
        if (!key_exists($id,  $this->avaliacoes)) {
            $this->avaliacoes[$id] = [];
        }
        $this->avaliacoes[$id][] = $avaliacao;
    }

    public function show()
    {
        return $this->avaliacoes;
    }
}


$corona = new Cerveja("Corona");
$skoll = new Cerveja("Skoll");
$repositorio = new Repositorio();

$repositorio->addAvaliacao($corona, new Avaliacao('SP', 5, 7));
$repositorio->addAvaliacao($skoll, new Avaliacao('SP', 4, 6));
$repositorio->addAvaliacao($skoll, new Avaliacao('MG', 4, 6));
var_dump($repositorio);
