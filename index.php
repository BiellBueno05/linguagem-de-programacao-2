<?php

    $contagemAulas = 0;
    function contagemAulas( int $contagemAulas)
    {
        $contagemAulas += 1;
        return $contagemAulas;
    }
?>  

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aulas</title>
</head>
<body>
    <h1>Linguagem de Programação II</h1>
    
    <p> Aula - 
        <?php 
            echo contagemAulas($contagemAulas) . "<br>"; 
            require_once './Textos/ColaDocker.txt';
        ?>
    </p>
</body>
</html>